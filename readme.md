# Simple UI components for react projects

This project contains some input components which can be used in your react flows. It contains components to get the most types of data from the user in an easy and intuitive manner.

## 1. Components

<hr>

### Bored Button

This is a button with a special effect on hover in case you are tired of the default look and feel of buttons.

### Date Picker

An input which takes a date from the user. It presents a field for entering the date manually or a widget where the user can select a date. The date is auto formatted when entered manually.

### Time Picker

An input which takes a time from the user. The user can enter the time in the input field and is auto formatted.

### Flex Selector

The flex selector gives the client a list of options to choose from. The presentation of the choices can be a html element, react element or plain text. Also the values corresponding to these entries can be any value.

### Text Line

This is the simple text input. You can choose between a couple of types for this component.

## 2. Validation

<hr>

This module also contains a small validation library to validate the input of the components. If desired you can use these in your project in conjunction with the validation of the components.


TODO - Timepicker masking
       Timepicker widget
       Flex selector - default styling
       Datepicker next prev buttons for years
       Datepicker masking
       Selector group by